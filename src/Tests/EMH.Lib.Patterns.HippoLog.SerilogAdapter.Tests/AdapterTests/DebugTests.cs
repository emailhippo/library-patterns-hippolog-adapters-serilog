﻿// <copyright file="DebugTests.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.Lib.Patterns.HippoLog.SerilogAdapter.Tests.AdapterTests
{
    using System;
    using System.Diagnostics;
    using FluentAssertions;
    using NUnit.Framework;
    using Serilog;
    using Serilog.Events;
    using Serilog.Sinks.TestCorrelator;

    /// <summary>
    /// The Debug Tests.
    /// </summary>
    [TestFixture]
    public sealed class DebugTests : TestBase
    {
        /// <summary>
        /// Logs the debug when logged with message expect log output.
        /// </summary>
        [Test]
        public void LogDebug_WhenLoggedWithMessage_ExpectLogOutput()
        {
            using (TestCorrelator.CreateContext())
            {
                // Arrange
                Log.Logger = new LoggerConfiguration()
                    .Enrich.FromLogContext()
                    .WriteTo.TestCorrelator()
                    .WriteTo.Console()
                    .MinimumLevel.Debug()
                    .CreateLogger();

                var serilogAdapter = SerilogAdapterFactory.Create();

                // Act
                var stopwatch = Stopwatch.StartNew();
                serilogAdapter.LogDebug(new DivideByZeroException("Error!"), "My error");
                stopwatch.Stop();

                // Assert
                WriteTimeElapsed(stopwatch);

                TestCorrelator.GetLogEventsFromCurrentContext()
                    .Should().ContainSingle()
                    .Which.MessageTemplate.Text
                    .Should().Be("My error");

                TestCorrelator.GetLogEventsFromCurrentContext()
                    .Should().ContainSingle()
                    .Which.Level
                    .Should().Be(LogEventLevel.Debug);
            }
        }
    }
}