﻿// <copyright file="TestBase.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.Lib.Patterns.HippoLog.SerilogAdapter.Tests
{
    using System;
    using System.Diagnostics;

    /// <summary>
    /// The Test Base.
    /// </summary>
    public abstract class TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase" /> class.
        /// </summary>
        protected TestBase()
        {
        }

        /// <summary>
        /// Writes the time elapsed.
        /// </summary>
        /// <param name="sw">The sw.</param>
        protected static void WriteTimeElapsed(Stopwatch sw)
        {
            Console.WriteLine($"Elapsed: {sw.ElapsedMilliseconds}ms ({sw.Elapsed})");
        }
    }
}