﻿// <copyright file="SerilogAdapterFactory.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.Lib.Patterns.HippoLog.SerilogAdapter
{
    using System;
    using EMH.Lib.Patterns.HippoLog.Core.Interfaces;
    using EMH.Lib.Patterns.HippoLog.SerilogAdapter.Logic;

    /// <summary>
    /// The Serilog Adapter Factory.
    /// </summary>
    public static class SerilogAdapterFactory
    {
        /// <summary>
        /// The lazy.
        /// </summary>
        private static readonly Lazy<ILogAdapter> Lazy = new Lazy<ILogAdapter>(() => new SerilogAdapter());

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>The <see cref="ILogAdapter"/>.</returns>
        public static ILogAdapter Create()
        {
            return Lazy.Value;
        }
    }
}