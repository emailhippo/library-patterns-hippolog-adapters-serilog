﻿// <copyright file="SerilogAdapter.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.Lib.Patterns.HippoLog.SerilogAdapter.Logic
{
    using System;
    using EMH.Lib.Patterns.HippoLog.Core.Interfaces;
    using Serilog;

    /// <inheritdoc />
    public sealed class SerilogAdapter : ILogAdapter
    {
        /// <inheritdoc />
        public void LogDebug(string messageTemplate)
            => Log.Debug(messageTemplate);

        /// <inheritdoc />
        public void LogDebug<T>(string messageTemplate, T propertyValue)
            => Log.Debug(messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogDebug<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Debug(messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogDebug<T0, T1, T2>(
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Debug(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogDebug(string messageTemplate, params object[] propertyValues)
            => Log.Debug(messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogDebug(Exception exception, string messageTemplate)
            => Log.Debug(exception, messageTemplate);

        /// <inheritdoc />
        public void LogDebug<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Debug(exception, messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogDebug<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Debug(exception, messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogDebug<T0, T1, T2>(
            Exception exception,
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Debug(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogDebug(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Debug(exception, messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogError(string messageTemplate) => Log.Error(messageTemplate);

        /// <inheritdoc />
        public void LogError<T>(string messageTemplate, T propertyValue)
            => Log.Error(messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogError<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Error(messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogError<T0, T1, T2>(
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Error(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogError(string messageTemplate, params object[] propertyValues)
            => Log.Error(messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogError(Exception exception, string messageTemplate)
            => Log.Error(exception, messageTemplate);

        /// <inheritdoc />
        public void LogError<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Error(exception, messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogError<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Error(exception, messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogError<T0, T1, T2>(
            Exception exception,
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Error(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogError(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Error(exception, messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogFatal(string messageTemplate)
            => Log.Fatal(messageTemplate);

        /// <inheritdoc />
        public void LogFatal<T>(string messageTemplate, T propertyValue)
            => Log.Fatal(messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogFatal<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Fatal(messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogFatal<T0, T1, T2>(
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Fatal(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogFatal(string messageTemplate, params object[] propertyValues)
            => Log.Fatal(messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogFatal(Exception exception, string messageTemplate)
            => Log.Fatal(exception, messageTemplate);

        /// <inheritdoc />
        public void LogFatal<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Fatal(exception, messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogFatal<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Fatal(exception, messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogFatal<T0, T1, T2>(
            Exception exception,
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Fatal(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogFatal(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Fatal(exception, messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogInformation(string messageTemplate)
            => Log.Information(messageTemplate);

        /// <inheritdoc />
        public void LogInformation<T>(string messageTemplate, T propertyValue)
            => Log.Information(messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogInformation<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Information(messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogInformation<T0, T1, T2>(
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Information(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogInformation(string messageTemplate, params object[] propertyValues)
            => Log.Information(messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogInformation(Exception exception, string messageTemplate)
            => Log.Information(exception, messageTemplate);

        /// <inheritdoc />
        public void LogInformation<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Information(exception, messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogInformation<T0, T1>(
            Exception exception,
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1)
            => Log.Information(exception, messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogInformation<T0, T1, T2>(
            Exception exception,
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Information(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogInformation(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Information(exception, messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogVerbose(string messageTemplate)
            => Log.Verbose(messageTemplate);

        /// <inheritdoc />
        public void LogVerbose<T>(string messageTemplate, T propertyValue)
            => Log.Verbose(messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogVerbose<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Verbose(messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogVerbose<T0, T1, T2>(
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Verbose(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogVerbose(string messageTemplate, params object[] propertyValues)
            => Log.Verbose(messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogVerbose(Exception exception, string messageTemplate)
            => Log.Verbose(exception, messageTemplate);

        /// <inheritdoc />
        public void LogVerbose<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Verbose(exception, messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogVerbose<T0, T1>(
            Exception exception,
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1)
            => Log.Verbose(exception, messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogVerbose<T0, T1, T2>(
            Exception exception,
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Verbose(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogVerbose(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Verbose(exception, messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogWarning(string messageTemplate)
            => Log.Warning(messageTemplate);

        /// <inheritdoc />
        public void LogWarning<T>(string messageTemplate, T propertyValue)
            => Log.Warning(messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogWarning<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
            => Log.Warning(messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogWarning<T0, T1, T2>(
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Warning(messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogWarning(string messageTemplate, params object[] propertyValues)
            => Log.Warning(messageTemplate, propertyValues);

        /// <inheritdoc />
        public void LogWarning(Exception exception, string messageTemplate)
            => Log.Warning(exception, messageTemplate);

        /// <inheritdoc />
        public void LogWarning<T>(Exception exception, string messageTemplate, T propertyValue)
            => Log.Warning(exception, messageTemplate, propertyValue);

        /// <inheritdoc />
        public void LogWarning<T0, T1>(
            Exception exception,
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1)
            => Log.Warning(exception, messageTemplate, propertyValue0, propertyValue1);

        /// <inheritdoc />
        public void LogWarning<T0, T1, T2>(
            Exception exception,
            string messageTemplate,
            T0 propertyValue0,
            T1 propertyValue1,
            T2 propertyValue2)
            => Log.Warning(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);

        /// <inheritdoc />
        public void LogWarning(Exception exception, string messageTemplate, params object[] propertyValues)
            => Log.Warning(exception, messageTemplate, propertyValues);
    }
}