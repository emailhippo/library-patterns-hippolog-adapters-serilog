# Serilog Adapter for HippoLog

## About
This is an adapter for [HippoLog Core](https://bitbucket.org/emailhippo/library-patterns-hippolog-core).

It is designed to be created and instantiated at the host level.

## Installation

Install using MyGet

```powershell
Install-Package EMH.Lib.Patterns.HippoLog.SerilogAdapter
```

## Usage

Initialize Serilog as normal.

```csharp
Log.Logger = new LoggerConfiguration()
    .Enrich.FromLogContext()
    .WriteTo.Console()
    .MinimumLevel.Debug()
    .CreateLogger();
```

Initialize the HippoLog with the Serilog Adapter

```csharp
ILogAdapter serilogAdapter = SerilogAdapterFactory.Create();

HippoLog.Initialize(serilogAdapter);

HippoLog.LogVerbose("I work!");
```

## Log format

See [Serilog docs](https://github.com/serilog/serilog/wiki/Writing-Log-Events) for format.

## See Also

See [HippoLog Core](https://bitbucket.org/emailhippo/library-patterns-hippolog-core)